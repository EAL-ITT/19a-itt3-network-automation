---
Week: 44
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 44 - 45 Network Automation

### Practical goals

* See Assignment in Deliverables

### Learning goals

The student can

* Explain how to retrieve a configuration by means of a Python program from a Junos box.

* Explain how to push a configuration by means of a Python program to a Junos box.

* Write a Python program to retrieve a configuration by means of a Python program from a Junos box.

* Write a Python program to push a configuration by means of a Python program to a Junos box.

* Document the above mentioned programs and validate the functionality of the programs.

### Topics to be covered

* Retrieve and pushing configuration from and to a Junos box.  

## Deliverables

* Assignment 30 Python, PyEZ and Junos configuration

## Schedule

* Thursdays and Friday week 43 to week 50

## Hands-on time

* See deliverables.

## Comments

TBD

## Sources

* 16S Python and JUNOS Juniper PyEZ pda Vxx.pdf

*   https://www.juniper.net/documentation/en_US/junos-pyez/topics/task/program/junos-pyez-program-configuration-retrieving.html  

*  https://www.juniper.net/documentation/en_US/junos-pyez/topics/reference/general/junos-pyez-configuration-process-and-data-formats.html


## White Board

TBD

