---
Week: 49
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 49 Network Automation Ansible

#### Practical goals

* See Assignment in Deliverables

#### Learning goals

The student can

* Work with Junos, RPC, NETCONF, and XML.
* Set up SSH authentication keys and use them with Ansible.

#### Topics to be covered and worked with

From the book listed in literature work through chapters:

* Chapter 5: Junos, RPC, NETCONF, and XML
* Chapter 6: Using SSH Keys

Do the following tasks:

* Test Junos, RPC, NETCONF, and XML SSH keys by  
creating basic play books and executing them:
    * Create and run the uptime.yaml version 2.0 to 2.3.
    * Create and run the Interfaces Version 1.0 to 1.4.
    * Create and run the uptime.yaml version 3.0. 

#### Deliverables

* Make a Network diagram.
* Demonstarte to the lecturer when playbooks 2.3, 1.4 and 3.0 are working.

#### Schedule

* Thursdays and Friday week 43 to week 50

#### Hands-on time

* See deliverables.

#### Comments

None

#### Sources

* Day One AUTOMATING JUNOS WITH ansible 2ndEd PDA notes.pdf  
Find it on LMS.

#### White Board

TBD