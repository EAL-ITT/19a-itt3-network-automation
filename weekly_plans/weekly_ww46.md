---
Week: 46
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 46 Network Automation

### Practical goals

* See Assignment in Deliverables

### Learning goals

The student can

* Work with Paramiko for remote acces to servers.

### Topics to be covered

* Retrieve and pushing configuration from and to a Junos box.  

## Deliverables

* Assignment 36 Python SSH and Junos with paramiko

## Schedule

* Thursdays and Friday week 43 to week 50

## Hands-on time

* See deliverables.

## Comments

TBD

## Sources

* mPython Network programming Paramiko p 67 - 86 PDA V10.pdf

* https://medium.com/@keagileageek/paramiko-how-to-ssh-and-file-transfers-with-python-75766179de73
* http://docs.paramiko.org/en/2.6/index.html
* https://github.com/paramiko/paramiko

## White Board

TBD

