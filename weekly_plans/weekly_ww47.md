---
Week: 47
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 47 Network Automation

### Practical goals

* See Assignment in Deliverables

### Learning goals

The student can

* Work with NetMiko for remote acces to servers.

### Topics to be covered

* Retrieve and pushing configuration from and to a Junos box.  

## Deliverables

* Assignment 37 Python Junos with NetMiko

## Schedule

* Thursdays and Friday week 43 to week 50

## Hands-on time

* See deliverables.

## Comments

TBD

## Sources

* Python Network programming Paramiko p 83 - 84 PDA V10.pdf

* https://medium.com/@keagileageek/paramiko-how-to-ssh-and-file-transfers-with-python-75766179de73
* https://www.linuxjournal.com/content/use-case-network-automation


## White Board

TBD

