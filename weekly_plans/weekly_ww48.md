---
Week: 48
Content:  COURSE WEEK CONTENT
Material: See links in weekly plan
Initials: PDA
---

# Week 48 Network Automation

#### Practical goals

* See Assignment in Deliverables

#### Learning goals

The student can

* Install Ansible on Linux.
* At a basic level work with JSON and YAML.
* C#reate an basic play book and execute it.
* Repeating a Playbook for Devices with Errors.
* Set up Ansible logging to a file.

#### Topics to be covered and worked with

From the book listed in literature work through chapters:

* Chapter 2: Installing Ansible
* Chapter 3: Understanding JSON and YAML
* Chapter 4: Running a Command – Your First Playbook

Do the following tasks:

* Install Ansible on Linux.
* Test JSON and YAML.
* Create a basic play book and execute it.  
    * Create and run the uptime.yaml version 1.1.
    * Create and run the uptime.yaml version 1.2.
    * Create and run the uptime.yaml version 1.3.
    * Repeating a Playbook for Devices with Errors.
* Set up Ansible logging to a file.  

#### Deliverables

* Make a Network diagram.
* Demonstarte to the lecturer when a playbook is working.

#### Schedule

* Thursdays and Friday week 43 to week 50

#### Hands-on time

* See deliverables.

#### Comments

None

#### Sources

* Day One AUTOMATING JUNOS WITH ansible 2ndEd PDA notes.pdf  
Find it on LMS.

#### White Board

TBD