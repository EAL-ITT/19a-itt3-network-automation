![Build Status](https://gitlab.com/EAL-ITT/19a-itt3-Network-Automation/badges/master/pipeline.svg)


# 19A-itt3-Network-Automation

weekly plans, resources and other relevant stuff for the project in IT technology 3. semester autumn.

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/19a-itt3-network-automation)